package mubaloo.dagger.daggersimple2.bugsense;

import android.content.Context;

/**
 * Contract for items of our classes wanting to log bugs
 */
public interface ICanStartBugLoggingSessions {

    /**
     * Initialize bug logger
     * @param context The app context
     */
    public void init(Context context);

    public String getBugSenseName();
}
