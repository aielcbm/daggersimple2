package mubaloo.dagger.daggersimple2.bugsense;

import android.content.Context;

import com.bugsense.trace.BugSenseHandler;

import mubaloo.dagger.daggersimple2.BuildConfig;


public class BugLoggingRelease implements ICanStartBugLoggingSessions {
    /**
     * Expect no op
     * @param context The app context
     */
    @Override
    public void init(Context context) {
        BugSenseHandler.initAndStartSession(context, BuildConfig.BUG_SENSE);
    }

    @Override
    public String getBugSenseName() {
        return "RELEASE";
    }
}
