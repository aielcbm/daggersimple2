package mubaloo.dagger.daggersimple2.bugsense;

import android.content.Context;

public class BugLoggingDebug implements ICanStartBugLoggingSessions {
    /**
     * Expect no op
     * @param context The app context
     */
    @Override
    public void init(Context context) {}

    @Override
    public String getBugSenseName() {
        return "DEBUG";
    }
}
