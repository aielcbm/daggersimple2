package mubaloo.dagger.daggersimple2.loaddata;

import android.database.sqlite.SQLiteDatabase;

import java.util.List;

public abstract class DataSource<T> {
    protected SQLiteDatabase mDatabase;

    public DataSource(SQLiteDatabase database) {
        mDatabase = database;
    }

    // don't need this for Dagger's example
//    public abstract boolean insert(T entity);
//    public abstract boolean delete(T entity);
//    public abstract boolean update(T entity);
//    public abstract List read(String selection, String[] selectionArgs, String groupBy, String having, String orderBy);
    public abstract List<T> read();
}
