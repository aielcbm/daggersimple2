package mubaloo.dagger.daggersimple2.loaddata;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mubaloo.dagger.daggersimple2.database.ServicesDatabaseOpenHelper;

public class DaggerDataSource extends DataSource<String> {
    public static final String TBL_SERVICES = "tbl_services";// table name
    public static final String COL_DESCRIPTION = "description";
    private static final String[] DAGGER_DEMO_COLS = new String[]{ COL_DESCRIPTION };

    @Inject
    DaggerDataSource(final ServicesDatabaseOpenHelper serviceDatabaseHelper) {
        super(serviceDatabaseHelper.openToRead());
    }

    @Override
    public List<String> read() {
        Cursor cursor = mDatabase.query(TBL_SERVICES, DAGGER_DEMO_COLS, null, null, null, null, null);
        int indexDescription = cursor.getColumnIndex(COL_DESCRIPTION);
        List<String> salekleenList = new ArrayList<String>();

        if (cursor != null && cursor.moveToFirst()) {
            do {
                salekleenList.add(cursor.getString(indexDescription));
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
            cursor.close();
        }

        return salekleenList;
    }
}
