package mubaloo.dagger.daggersimple2.secondactivity;

import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import mubaloo.dagger.daggersimple2.BaseActivity;
import mubaloo.dagger.daggersimple2.R;
import mubaloo.dagger.daggersimple2.bugsense.ICanStartBugLoggingSessions;
import mubaloo.dagger.daggersimple2.loaddata.DaggerDataLoader;
import mubaloo.dagger.daggersimple2.loaddata.DaggerDataSource;
import mubaloo.dagger.daggersimple2.modules.SecondActivityModule;

public class SecondActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<List<String>> {
    private static final int LOADER_ID = 100;
    @Inject DaggerDataSource mDaggerDataSource;
    @Inject LocationManager locationManager;
    @Inject ICanStartBugLoggingSessions mICanStartBugLoggingSessions;
    private ArrayAdapter<String> mAdapter;

    /**
     * A list of modules to use for the individual activity graph. Subclasses can override this
     * method to provide additional modules provided they call and include the modules returned by
     * calling {@code super.getModules()}.
     */
    @Override
    protected List<Object> getModules() {
        return Arrays.<Object>asList(new SecondActivityModule(this));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity_layout);

        ListView listView = (ListView) findViewById(R.id.second_activity_list);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(android.R.id.content, SecondActivityFragment.newInstance(mICanStartBugLoggingSessions.getBugSenseName()))
                    .commit();
        }
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        listView.setAdapter(mAdapter);

        getSupportLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public Loader<List<String>> onCreateLoader(int id, Bundle args) {
        return new DaggerDataLoader(this, mDaggerDataSource);
    }

    @Override
    public void onLoadFinished(Loader<List<String>> loader, List<String> data) {
        mAdapter.clear();
        for (String name : data){
            mAdapter.add(name);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<String>> loader) {
        mAdapter.clear();
    }
}
