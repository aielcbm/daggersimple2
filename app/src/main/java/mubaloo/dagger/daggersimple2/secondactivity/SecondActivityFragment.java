package mubaloo.dagger.daggersimple2.secondactivity;

import android.os.Bundle;

import javax.inject.Inject;

import mubaloo.dagger.daggersimple2.DemoBaseFragment;
import mubaloo.dagger.daggersimple2.ActivityTitleController;

public class SecondActivityFragment extends DemoBaseFragment {
    private final static String BUG_SENSE_VERSION = "bugSenseVersion";
    @Inject ActivityTitleController titleController;

    public static SecondActivityFragment newInstance(String bugSenseVersion) {
        SecondActivityFragment fragment = new SecondActivityFragment();
        Bundle args = new Bundle();
        args.putString(BUG_SENSE_VERSION,bugSenseVersion);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();

        Bundle bundle = getArguments();

        if (bundle != null && bundle.containsKey(BUG_SENSE_VERSION)) {
            // Fragments should not modify things outside of their own view. Use an external controller to
            // ask the activity to change its title.
            titleController.setTitle("Bug Sense version: " + bundle.getString(BUG_SENSE_VERSION));
        }
    }
}