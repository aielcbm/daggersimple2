/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mubaloo.dagger.daggersimple2.firstactivity;

import android.location.LocationManager;
import android.os.Bundle;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import mubaloo.dagger.daggersimple2.BaseActivity;
import mubaloo.dagger.daggersimple2.bugsense.ICanStartBugLoggingSessions;
import mubaloo.dagger.daggersimple2.modules.FirstActivityModule;

public class FirstActivity extends BaseActivity {
    @Inject LocationManager locationManager;
    @Inject ICanStartBugLoggingSessions mICanStartBugLoggingSessions;

    /**
     * A list of modules to use for the individual activity graph. Subclasses can override this
     * method to provide additional modules provided they call and include the modules returned by
     * calling {@code super.getModules()}.
     */
    @Override
    protected List<Object> getModules() {
        return Arrays.<Object>asList(new FirstActivityModule(this));
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // After the super.onCreate call returns we are guaranteed our injections are available.

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
            .add(android.R.id.content, FirstActivityFragment.newInstance(mICanStartBugLoggingSessions.getBugSenseName()))
            .commit();
        }

        // TODO do something with the injected dependencies here!
    }
}
