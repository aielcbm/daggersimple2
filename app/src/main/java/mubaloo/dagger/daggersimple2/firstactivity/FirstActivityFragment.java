/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mubaloo.dagger.daggersimple2.firstactivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import javax.inject.Inject;

import mubaloo.dagger.daggersimple2.ActivityTitleController;
import mubaloo.dagger.daggersimple2.DemoBaseFragment;
import mubaloo.dagger.daggersimple2.R;
import mubaloo.dagger.daggersimple2.secondactivity.SecondActivity;

public class FirstActivityFragment extends DemoBaseFragment {
    private final static String BUG_SENSE_VERSION = "bugSenseVersion";
    @Inject
    ActivityTitleController titleController;

    public static FirstActivityFragment newInstance(String bugSenseVersion) {
        FirstActivityFragment fragment = new FirstActivityFragment();
        Bundle args = new Bundle();
        args.putString(BUG_SENSE_VERSION,bugSenseVersion);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.first_activity_fragment_layout,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button button = (Button) view.findViewById(R.id.first_activity_fragment_button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SecondActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        Bundle bundle = getArguments();

        if (bundle != null && bundle.containsKey(BUG_SENSE_VERSION)) {
            // Fragments should not modify things outside of their own view. Use an external controller to
            // ask the activity to change its title.
            titleController.setTitle("Bug Sense version: " + bundle.getString(BUG_SENSE_VERSION));
        }
    }
}
