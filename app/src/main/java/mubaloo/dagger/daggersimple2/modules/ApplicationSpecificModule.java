/*
 * Copyright (C) 2013 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mubaloo.dagger.daggersimple2.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import mubaloo.dagger.daggersimple2.BuildConfig;
import mubaloo.dagger.daggersimple2.bugsense.BugLoggingDebug;
import mubaloo.dagger.daggersimple2.bugsense.BugLoggingRelease;
import mubaloo.dagger.daggersimple2.bugsense.ICanStartBugLoggingSessions;

@Module(
        complete = true,
        addsTo = AndroidModule.class,
        library = true
)
public class ApplicationSpecificModule {
    // TODO put your application-specific providers here!

    @Provides @Singleton ICanStartBugLoggingSessions providesBugLogger () {
        if ("debug".equals(BuildConfig.BUG_SENSE)) {
            return new BugLoggingDebug();
        } else {
            return new BugLoggingRelease();
        }
    }
}
