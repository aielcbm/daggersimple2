package mubaloo.dagger.daggersimple2.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import mubaloo.dagger.daggersimple2.ForActivity;
import mubaloo.dagger.daggersimple2.ActivityTitleController;
import mubaloo.dagger.daggersimple2.secondactivity.SecondActivity;
import mubaloo.dagger.daggersimple2.secondactivity.SecondActivityFragment;

/**
 * This module represents objects which exist only for the scope of a single activity. We can
 * safely create singletons using the activity instance because the entire object graph will only
 * ever exist inside of that activity.
 */
@Module(
        injects = {
                SecondActivity.class,
                SecondActivityFragment.class
        },
        addsTo = ApplicationSpecificModule.class,
        complete = true,
        library = true
)
public class SecondActivityModule {
    private final SecondActivity activity;

    public SecondActivityModule(SecondActivity activity) {
        this.activity = activity;
    }

    /**
     * Allow the activity context to be injected but require that it be annotated with
     * {@link mubaloo.dagger.daggersimple2.ForActivity @ForActivity} to explicitly differentiate it from application context.
     */
    @Provides @Singleton @ForActivity Context provideActivityContext() {
        return activity;
    }

    @Provides @Singleton ActivityTitleController provideTitleController() {
        return new ActivityTitleController(activity);
    }
}