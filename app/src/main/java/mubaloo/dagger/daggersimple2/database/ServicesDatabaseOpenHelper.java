package mubaloo.dagger.daggersimple2.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import javax.inject.Inject;

import mubaloo.dagger.daggersimple2.ForActivity;

public class ServicesDatabaseOpenHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "services.db";
    private SQLiteDatabase mDB;
    public static final String TBL_SERVICES = "tbl_services";// table name
    public static final String COL_ID = "_id";
    public static final String COL_DESCRIPTION = "description";
    public static final String COL_TYPE = "type";
    public static final String COL_COUNTRY = "country";
    public static final String COL_DISCOUNT_PERMITTED = "discount_permitted";
    public static final String COL_MAX_DISCOUNT = "max_discount";
    public static final String COL_MACHINE = "machine";
    public static final String COL_INTERVAL_1 = "interval_1";
    public static final String COL_INTERVAL_2 = "interval_2";
    public static final String COL_INTERVAL_3 = "interval_3";
    public static final String COL_INTERVAL_4 = "interval_4";
    public static final String COL_INTERVAL_6 = "interval_6";
    public static final String COL_INTERVAL_8 = "interval_8";
    public static final String COL_INTERVAL_9 = "interval_9";
    public static final String COL_INTERVAL_12 = "interval_12";
    public static final String COL_INTERVAL_16 = "interval_16";
    public static final String COL_INTERVAL_24 = "interval_24";
    public static final String COL_INTERVAL_52 = "interval_52";
    public static final String COL_ENVIRO_CODE = "enviro_code";

    private static final String DATABASE_CREATE = "create table " +
            TBL_SERVICES + " (" +
            COL_ID + " integer primary key, " +
            COL_DESCRIPTION + " text not null, " +
            COL_TYPE + " text not null, " +
            COL_COUNTRY + " text not null, " +
            COL_DISCOUNT_PERMITTED + " integer not null, " +
            COL_MAX_DISCOUNT + " float not null, " +
            COL_MACHINE + " integer not null, " +
            COL_INTERVAL_1 + " float not null, " +
            COL_INTERVAL_2 + " float not null, " +
            COL_INTERVAL_3 + " float not null, " +
            COL_INTERVAL_4 + " float not null, " +
            COL_INTERVAL_6 + " float not null, " +
            COL_INTERVAL_8 + " float not null, " +
            COL_INTERVAL_9 + " float not null, " +
            COL_INTERVAL_12 + " float not null, " +
            COL_INTERVAL_16 + " float not null, " +
            COL_INTERVAL_24 + " float not null, " +
            COL_INTERVAL_52 + " float not null, " +
            COL_ENVIRO_CODE + " integer not null);";

    @Inject
    ServicesDatabaseOpenHelper(@ForActivity Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
        db.beginTransaction();
        for (String statement : ServiceDatabaseInsert.INSERT_STATEMENTS) {
            db.execSQL(statement);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(ServicesDatabaseOpenHelper.class.getName(), "Updating database from version " + oldVersion + " to " + newVersion);
    }

    public SQLiteDatabase openToRead() throws SQLException {
        mDB = getReadableDatabase();
        return mDB;
    }

    @Override
    public void close() {
        try {
            mDB.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}